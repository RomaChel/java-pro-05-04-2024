package org.example.hw2;

public class Cat extends Animal {
    private static int count = 0;

    public Cat() {
        count++;
    }

    public static int getCount() {
        return count;
    }

    @Override
    public void run(int distance) {
        if (distance <= 200) {
            System.out.println("Cat runs " + distance + " meters");
        } else {
            System.out.println("Cat can't run that far");
        }
    }

    @Override
    public void swim(int distance) {
        System.out.println("Cat can't swim");
    }
}
