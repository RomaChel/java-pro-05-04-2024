package org.example.hw2;

public class Animal {

    public void run(int distance) {
        System.out.println("Animal runs " + distance + " meters");
    }

    public void swim(int distance) {
        System.out.println("Animal swims " + distance + " meters");
    }
}
