package org.example;

import org.example.hw2.Cat;
import org.example.hw2.Dog;

public class Main {
    public static void main(String[] args) {
        Dog dogBobik = new Dog();
        dogBobik.run(400);
        dogBobik.swim(5);

        Cat catMorik = new Cat();
        catMorik.run(150);
        catMorik.swim(3);

        System.out.println("Number of dogs Bobik: " + Dog.getCount());
        System.out.println("Number of cats Morik: " + Cat.getCount());
    }
}